from s3 import S3
from db import Db
import assemblyai as aai
import logging
import json


def main():
    print("=================================================================================")
    print('entered to main of main')
    print("=================================================================================")
    # logging.basicConfig(filename="log.txt", level=logging.DEBUG)
    # logging.debug("Debug logging test...")
    # aai.settings.api_key = "723133c96920496f910dc9f36b940208"
    # config=aai.TranscriptionConfig(sentiment_analysis=True, speaker_labels=True)

    # transcriber = aai.Transcriber()
    aws = S3
    db = Db
    file_names = db.get_files()
    finalResult = []
    for name in file_names:
        d = aws.download_file(name)
        FILE_URL = name
        print("name of one file in main: ")
        print(FILE_URL)
        if d:
            aai.settings.api_key = "723133c96920496f910dc9f36b940208"
            config=aai.TranscriptionConfig(sentiment_analysis=True, speaker_labels=True)

            transcriber = aai.Transcriber()
            transcript = transcriber.transcribe(FILE_URL,config=config)
        #    sql = db.update(name[0], result)
            result = transcript.sentiment_analysis
            print(result)

            # Convert Sentiment objects to a list of dictionaries
            sentiments_as_dicts = []
            for sentiment in result:
                sentiment_dict = {
                    'text': sentiment.text,
                    'start': sentiment.start,
                    'end': sentiment.end,
                    'confidence': sentiment.confidence,
                    'sentiment': sentiment.sentiment.name,  # Convert Enum to string
                    'speaker': sentiment.speaker
                }
                sentiments_as_dicts.append(sentiment_dict)

            # Convert the list of dictionaries to a JSON string
            result = json.dumps(sentiments_as_dicts)
            print(result)
            if query(name, result):
                print('data updated successfully :')
                print(transcript.json_response)
            else:
                print(f'error:  {transcript.error}')


def query(file_name,data):
        print("=================================================================================")
        print('enter to query of main')
        print("=================================================================================")
        db = Db 
        query  = db.update(file_name,data)
        if query > 1:
                print("=================================================================================")
                print('enter to query of main')
                print("=================================================================================")
                return True
        else:
                print("=================================================================================")
                print('enter to query of main')
                print("=================================================================================")
                return False

if __name__ == "__main__":
    main()  